
package com.egolden.wangdenghui.eventhandler.event.bean;

/**
 * 事件源
 *
 * @author wangdenghui E-mail:WANGDENGHUI351@pingan.com.cn
 * @version 1.1.0
 */
public class SourceBean {
    private String mName;
    private Object mSender;

    public SourceBean(String name) {
        this(name, null);
    }

    public SourceBean(String name, Object sender) {
        if (name == null || name.length() == 0) {
            throw new NullPointerException("The mEventName of EventSource cannot be empty");
        }
        mSender = sender;
        mName = name;
    }

    public String getName() {
        return mName;
    }

    public Object getSender() {
        return mSender;
    }

    public SourceBean(Class<?> clazz, Object sender) {
        this(clazz.getName(), sender);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((mName == null) ? 0 : mName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SourceBean other = (SourceBean) obj;
        if (mName == null) {
            if (other.mName != null)
                return false;
        } else if (!mName.equals(other.mName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "EventSource [mName=" + mName + ", mSender=" + mSender + "]";
    }

}
