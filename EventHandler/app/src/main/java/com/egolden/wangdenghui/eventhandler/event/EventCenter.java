
package com.egolden.wangdenghui.eventhandler.event;

import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;

import com.egolden.wangdenghui.eventhandler.event.bean.Event;
import com.egolden.wangdenghui.eventhandler.event.bean.ObserverBean;
import com.egolden.wangdenghui.eventhandler.event.bean.SourceBean;
import com.egolden.wangdenghui.eventhandler.event.datastruct.MultiSparseArray;
import com.egolden.wangdenghui.eventhandler.event.poster.AsyncPoster;
import com.egolden.wangdenghui.eventhandler.event.poster.BackgroundPoster;
import com.egolden.wangdenghui.eventhandler.event.poster.HandlerPoster;
import com.egolden.wangdenghui.eventhandler.event.poster.PendingPost;
import com.egolden.wangdenghui.eventhandler.event.tracer.EventTracer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * <pre>
 * 事件中心引入了EventBus的机制，同时强化了EventBus机制，EventBus本身不支持多个事件
 * </pre>
 *
 * @author wangdenghui E-mail:WANGDENGHUI351@pingan.com.cn
 * @version 1.1.0
 */
public class EventCenter {

    private static final String tag = "EventCenter";
    private static EventCenter mInstance;

    public static final EventCenter getInstance() {
        if (mInstance == null) {
            mInstance = new EventCenter();
        }
        return mInstance;
    }

    private final HandlerPoster mainThreadPoster;
    private final BackgroundPoster backgroundPoster;
    private final AsyncPoster asyncPoster;

    private ReadWriteLock lock = new ReentrantReadWriteLock();

    private ConcurrentHashMap<SourceBean, MultiSparseArray<ObserverBean>> observerMap = new ConcurrentHashMap<SourceBean, MultiSparseArray<ObserverBean>>();

    private List<EventInterceptor> eventInterceptors = Collections
            .synchronizedList(new ArrayList<EventInterceptor>());

    private EventTracer eventTracer;

    private final ThreadLocal<List<Object>> threadLocalEventQueue = new ThreadLocal<List<Object>>() {
        @Override
        protected List<Object> initialValue() {
            return new ArrayList<Object>();
        }
    };

    private final ThreadLocal<BooleanWrapper> currentThreadIsPosting = new ThreadLocal<BooleanWrapper>() {
        @Override
        protected BooleanWrapper initialValue() {
            return new BooleanWrapper();
        }
    };

    final static class BooleanWrapper {
        boolean value;
    }

    private EventCenter() {
        mainThreadPoster = new HandlerPoster(this, Looper.getMainLooper(), 10);
        backgroundPoster = new BackgroundPoster(this);
        asyncPoster = new AsyncPoster(this);
    }

    /**
     * Post线程处理Event<有可能执行在UI线程,也有可能执行在后台线程> 观察者在onEventPostThread里面处理Event
     *
     * @param observer
     * @param sourceName
     * @param whats
     */
    public void addObserver(IObserver observer, String sourceName, int... whats) {
        addObserver(EventMode.PostThread, observer, new SourceBean(sourceName), whats);
    }

    /**
     * Post线程处理Event<有可能执行在UI线程,也有可能执行在后台线程> 观察者在onEventPostThread里面处理Event
     *
     * @param observer
     * @param source：可以指定发送者对象，主要考虑到可能出现不同实例发送同一个Event的事件的特殊情况，所以指定发送者对象实例
     * @param whats
     */

    public void addObserver(IObserver observer, SourceBean source, int... whats) {
        addObserver(EventMode.PostThread, observer, source, whats);
    }

    /**
     * 根据指定模式threadMode执行Event
     *
     * @param observer
     * @param eventSourceName
     * @param threadMode
     * @param whats
     */
    public void addObserver(EventMode threadMode, IObserver observer, String eventSourceName,
                            int... whats) {
        addObserver(threadMode, observer, new SourceBean(eventSourceName), whats);
    }

    /**
     * 根据指定模式threadMode执行Event
     *
     * @param observer
     * @param source
     * @param threadMode
     * @param whats
     */
    public void addObserver(EventMode threadMode, IObserver observer, SourceBean source,
                            int... whats) {
        addObserverInner(threadMode, observer, source, whats);
    }

    /**
     * 添加支持不同线程模式的接口
     *
     * @param observer
     * @param source
     * @param threadMode
     * @param whats
     */
    private void addObserverInner(EventMode threadMode, Object observer,
                                  SourceBean source,
                                  int... whats) {
        if (observer == null) {
            throw new NullPointerException("observingObject must not be null!");
        }
        if (source == null || TextUtils.isEmpty(source.getName())) {
            throw new NullPointerException("you must specified eventSource!");
        }
        if (whats == null) {
            return;
        }

        Lock writeLock = lock.writeLock();
        try {
            writeLock.lock();
            ObserverBean observerBean = new ObserverBean(observer, source.getSender(),
                    threadMode);
            MultiSparseArray<ObserverBean> om = observerMap.get(source);
            if (om == null) {
                om = new MultiSparseArray<ObserverBean>();
                observerMap.put(source, om);
            }
            for (int what : whats) {
                om.put(what, observerBean);
            }
        } finally {
            writeLock.unlock();
        }
    }

    /**
     * 移除观察者observingObject注册的所有事件
     */
    public void removeObserver(Object observingObject) {
        removeObserver(observingObject, null);
    }

    /**
     * 移除观察者注册在事件源source下的所有事件
     */
    public void removeObserver(Object observingObject, SourceBean source) {
        if (observingObject == null) {
            throw new NullPointerException("observingObject must not be null");
        }

        Lock writeLock = lock.writeLock();
        try {
            writeLock.lock();
            if (source != null) {
                removeAllObserverByEventSource(observingObject, source);
            } else {
                Collection<SourceBean> collections = observerMap.keySet();
                if (collections != null) {
                    for (SourceBean _source : collections) {
                        removeAllObserverByEventSource(observingObject, _source);
                    }
                }
            }
        } finally {
            writeLock.unlock();
        }
    }

    /**
     * 移除观察者注册的指定事件
     */
    public void removeObserver(Object observingObject, SourceBean source, int... whats) {
        if (observingObject == null)
            throw new NullPointerException("observingObject must not be null");
        if (whats == null) {
            return;
        }

        Lock writeLock = lock.writeLock();
        try {
            writeLock.lock();
            if (source != null) {
                for (int what : whats) {
                    removeObserverByEventSource(observingObject, source, what);
                    Log.v(tag, "removeObserver observingObject2:" + observingObject.getClass()
                            + " whats:" + what);
                }
            } else {
                Collection<SourceBean> collections = observerMap.keySet();
                if (collections != null) {
                    for (SourceBean _source : collections) {
                        for (int what : whats) {
                            removeObserverByEventSource(observingObject, _source, what);
                        }
                    }
                }
            }
        } finally {
            writeLock.unlock();
        }
    }

    /**
     * 删除observingObject关心的所有source下的事件
     *
     * @param observingObject
     * @param source
     */
    private void removeAllObserverByEventSource(Object observingObject, SourceBean source) {
        MultiSparseArray<ObserverBean> om = observerMap.get(source);
        if (om != null) {
            int keySize = om.keySize();
            for (int i = 0; i < keySize; i++) {
                int what = om.keyAt(i);
                removeObserverFromCollection(om.get(what), observingObject);
            }
        }
    }

    private void removeObserverByEventSource(Object observingObject, SourceBean source, int what) {
        MultiSparseArray<ObserverBean> om = observerMap.get(source);
        if (om != null) {
            removeObserverFromCollection(om.get(what), observingObject);
        }
    }

    private void removeObserverFromCollection(Collection<ObserverBean> observers,
                                              Object observingObject) {
        if (observers != null) {
            Iterator<ObserverBean> iterator = observers.iterator();
            while (iterator.hasNext()) {
                ObserverBean observer = iterator.next();
                if (observer == null)
                    continue;
                Object tmpObject = observer.getObservingObject();
                if (tmpObject != null && tmpObject.equals(observingObject)) {
                    iterator.remove();
                }
            }
        }
    }

    public void setTracer(EventTracer tracer) {
        eventTracer = tracer;
    }

    private ArrayList<EventInterceptor> getEventInterceptors() {
        ArrayList<EventInterceptor> interceptors = new ArrayList<EventInterceptor>();
        for (EventInterceptor interceptor : eventInterceptors) {
            if (interceptor != null) {
                interceptors.add(interceptor);
            }
        }
        return interceptors;
    }

    private Collection<ObserverBean> getObserverBeans(Event event) {
        final SourceBean source = event.source;
        MultiSparseArray<ObserverBean> om = observerMap.get(source);
        if (om != null) {
            Collection<ObserverBean> observers = om.get(event.what);
            if (observers != null) {
                return new ArrayList<ObserverBean>(observers);
            }
        }
        return null;
    }

    public void post(SourceBean source, int what, Event.EventRank eventRank, Object parameters) {
        if (eventRank == null) {
            eventRank = Event.EventRank.NORMAL;
        }
        post(Event.generate(what, source, parameters, eventRank));
    }

    public void post(final Event event) {
        if (event == null) {
            throw new NullPointerException("Event cannot be null");
        }
        final SourceBean source = event.source;
        if (source == null || TextUtils.isEmpty(source.getName())) {
            throw new NullPointerException("EventSource cannot be null");
        }

        List<Object> eventQ = threadLocalEventQueue.get();
        eventQ.add(event);

        BooleanWrapper isPosting = currentThreadIsPosting.get();
        if (isPosting.value) {
            return;
        } else {
            boolean isMainThread = Looper.getMainLooper() == Looper.myLooper();
            isPosting.value = true;
            try {
                while (!eventQ.isEmpty()) {
                    postSingleEvent((Event) eventQ.remove(0), isMainThread);
                }
            } finally {
                isPosting.value = false;
            }
        }
    }

    public void post(SourceBean source, int what, Event.EventRank eventRank) {
        post(source, what, eventRank, null);
    }

    private void postSingleEvent(Event event, boolean isMainThread) throws Error {
        final ArrayList<EventInterceptor> interceptors;
        final Collection<ObserverBean> observerBeans;

        Lock readLock = lock.readLock();
        try {
            readLock.lock();
            interceptors = getEventInterceptors();
            observerBeans = getObserverBeans(event);
        } finally {
            readLock.unlock();
        }

        if (interceptors != null) {
            for (EventInterceptor interceptor : interceptors) {
                if (interceptor != null && interceptor.intercept(event)) {
                    return;
                }
            }
        }
        if (observerBeans != null) {
            for (final ObserverBean observer : observerBeans) {
                postToSubscription(observer, event, isMainThread);
            }
        }
    }

    private void postToSubscription(ObserverBean observer, Event event, boolean isMainThread) {
        switch (observer.mThreadMode) {
            case PostThread:
                invokeObserver(observer, event);
                break;
            case MainThread:
                if (isMainThread) {
                    invokeObserver(observer, event);
                } else {
                    mainThreadPoster.enqueue(observer, event);
                }
                break;
            case BackgroundThread:
                if (isMainThread) {
                    backgroundPoster.enqueue(observer, event);
                } else {
                    invokeObserver(observer, event);
                }
                break;
            case Async:
                asyncPoster.enqueue(observer, event);
                break;
            default:
                throw new IllegalStateException("Unknown thread mode: " + observer.mThreadMode);
        }
    }

    public void invokeObserver(PendingPost pendingPost) {
        Event event = (Event) pendingPost.event;
        ObserverBean observer = pendingPost.subscription;
        PendingPost.releasePendingPost(pendingPost);
        invokeObserver(observer, event);
    }

    private void invokeObserver(ObserverBean observerBean, Event event) {
        EventTracer tracer = eventTracer;
        if (observerBean.getObservingObject() == event.source.getSender()) {
            String msg = "EventCenter Warning>>>>Observer(" + observerBean.getObservingObject()
                    + ") == Sender(" + event.source.getSender() + " EventName:"
                    + event.source.getName() + " EventId:" + event.what + ")";
            Log.e(tag, msg);
        }
        if (tracer != null) {
            tracer.beforeEvent(observerBean.getObservingObject(), event);
        }
        observerBean.invoke(event);
        if (tracer != null) {
            tracer.afterEvent(observerBean.getObservingObject(), event);
        }
    }

    /**
     * 注册事件拦截器
     */
    public void addEventInterceptor(EventInterceptor interceptor) {
        if (interceptor == null) {
            throw new NullPointerException("interceptor cannot be null");
        }

        Lock writeLock = lock.writeLock();
        try {
            writeLock.lock();
            this.eventInterceptors.add(interceptor);
        } finally {
            writeLock.unlock();
        }
    }

    public void removeEventInterceptor(EventInterceptor interceptor) {

        Log.v(tag, "removeEventInterceptor:");

        Lock writeLock = lock.writeLock();
        try {
            writeLock.lock();
            this.eventInterceptors.remove(interceptor);
        } finally {
            writeLock.unlock();
        }
    }

}
