package com.egolden.wangdenghui.eventhandler.view;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.egolden.wangdenghui.eventhandler.R;
import com.egolden.wangdenghui.eventhandler.bean.UserInfo;
import com.egolden.wangdenghui.eventhandler.event.EventCenter;
import com.egolden.wangdenghui.eventhandler.event.bean.Event;
import com.egolden.wangdenghui.eventhandler.event.bean.SourceBean;
import com.egolden.wangdenghui.eventhandler.utils.Constans;

/**
 * (事件发起者)被观察者activity
 */
public class SubjectEventActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnLoginIn = (Button) findViewById(R.id.btn_login);
        btnLoginIn.setOnClickListener(this);
        Button btnLoginOut = (Button) findViewById(R.id.btn_login_out);
        btnLoginOut.setOnClickListener(this);
    }


    /**
     * 登录事件
     */
    public void sendLoginSuccessMsg() {
        UserInfo userInfo = new UserInfo("jack", "24", "man", "120");
        Event event = Event.generate(Constans.LoginEventModel.EVENT_LOGIN_IN, new SourceBean(Constans.LoginEventModel.Name, this), userInfo);
        EventCenter.getInstance().post(event);
    }


    /**
     * 退出登录事件
     */
    public void sendLoginOutSuccessMsg() {
        Event event = Event.generate(Constans.LoginEventModel.EVENT_LOGIN_OUT, new SourceBean(Constans.LoginEventModel.Name, this), "login out！");
        EventCenter.getInstance().post(event);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login:
                sendLoginSuccessMsg();
                break;
            case R.id.btn_login_out:
                sendLoginOutSuccessMsg();
                break;

        }
    }
}
