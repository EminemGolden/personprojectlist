/*
                   _ooOoo_
                  o8888888o
                  88" . "88
                  (| -_- |)
                  O\  =  /O
               ____/`---'\____
             .'  \\|     |//  `.
            /  \\|||  :  |||//  \
           /  _||||| -:- |||||-  \
           |   | \\\  -  /// |   |
           | \_|  ''\---/''  |   |
           \  .-\__  `-`  ___/-. /
         ___`. .'  /--.--\  `. . __
      ."" '<  `.___\_<|>_/___.'  >'"".
     | | :  `- \`.;`\ _ /`;.`/ - ` : | |
     \  \ `-.   \_ __\ /__ _/   .-` /  /
======`-.____`-.___\_____/___.-`____.-'======
                   `=---='
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
         佛祖保佑       永无BUG
 */

package com.egolden.wangdenghui.eventhandler.event.threadpool;


/**
 * <pre>
 * 测试用例
 * </pre>
 * 
 * @author chaseli
 * @date 2014年9月3日 下午1:40:49
 * @version 1.0
 */
public class demo {
    private class Result {

    };

    @SuppressWarnings("unused")
    private void defaultTest() {
        PriorityThreadPool.getDefault().submit(new ThreadPool.Job<Result>() {

            @Override
            public Result run(ThreadPool.JobContext jc) {
                // 添加取消监听
                jc.setCancelListener(new ThreadPool.CancelListener() {
                    @Override
                    public void onCancel() {
                        // TODO Auto-generated method stub

                    }
                });

                // 判断任务是否已经被取消
                Result rst = new Result();
                if (jc.isCancelled())
                    return rst;// 任务已经被取消

                // TODO 处理
                return rst;
            }

        }, new FutureListener<Result>() {

            @Override
            public void onFutureBegin(Future<Result> future) {
                // 开始执行任务
            }

            @Override
            public void onFutureDone(Future<Result> future) {
                // 任务执行完成
            }

        }, PriorityThreadPool.Priority.LOW);
    }
}
