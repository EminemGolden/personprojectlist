
package com.egolden.wangdenghui.eventhandler.event;


import com.egolden.wangdenghui.eventhandler.event.bean.Event;

/**
 * <pre>
 * 事件拦截器
 * </pre>
 *
 * @author wangdenghui E-mail:WANGDENGHUI351@pingan.com.cn
 * @version 1.1.0
 */
public interface EventInterceptor {

    /**
     * 拦截事件方法
     *
     * @param event 事件
     * @return 返回true代表拦截，false继续往下传递
     */
    public boolean intercept(Event event);

}
