
package com.egolden.wangdenghui.eventhandler.event.demo;


import com.egolden.wangdenghui.eventhandler.event.EventCenter;
import com.egolden.wangdenghui.eventhandler.event.bean.Event;
import com.egolden.wangdenghui.eventhandler.event.bean.SourceBean;

/**
 * <pre>
 * 事件发送点
 * </pre>
 *
 * @author chaseli
 * @version 1.0
 * @date 2014年7月29日 下午2:15:46
 */
public class SourceModel {
    private static SourceModel mInstance;

    public static final SourceModel getInstance() {
        if (mInstance == null) {
            mInstance = new SourceModel();
        }
        return mInstance;
    }

    public void sendMessage() {
        Event event = Event.generate(DemoEventConstant.DemoModel.EVENT_1, new SourceBean(DemoEventConstant.DemoModel.Name, this), "sender event1");
        EventCenter.getInstance().post(event);
    }
}
