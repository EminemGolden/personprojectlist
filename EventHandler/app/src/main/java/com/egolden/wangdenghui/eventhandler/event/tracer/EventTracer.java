
package com.egolden.wangdenghui.eventhandler.event.tracer;


import com.egolden.wangdenghui.eventhandler.event.bean.Event;

/**
 * <pre>
 * 事件监听
 * </pre>
 * @author wangdenghui E-mail:WANGDENGHUI351@pingan.com.cn
 *
 * @version 1.1.0
 */
public interface EventTracer {

    void beforeEvent(Object observer, Event event);

    void afterEvent(Object observer, Event event);
}
