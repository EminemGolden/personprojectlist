
package com.egolden.wangdenghui.eventhandler.event.threadpool;

public interface FutureListener<T> {
     void onFutureBegin(Future<T> future);

     void onFutureDone(Future<T> future);
}
