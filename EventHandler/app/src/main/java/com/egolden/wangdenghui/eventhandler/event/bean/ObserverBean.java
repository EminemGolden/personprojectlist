
package com.egolden.wangdenghui.eventhandler.event.bean;

import android.util.Log;

import com.egolden.wangdenghui.eventhandler.event.EventMode;
import com.egolden.wangdenghui.eventhandler.event.IObserver;

import java.lang.ref.WeakReference;


/**
 * @author wangdenghui E-mail:WANGDENGHUI351@pingan.com.cn
 * @version 1.1.0
 */
public class ObserverBean {

    private static final String tag = "ObserverBean";

    public final EventMode mThreadMode;

    private final WeakReference<Object> observerReference;

    private final WeakReference<Object> senderReference;

    private final int mSenderHashCode;
    private final int mObserverHashCode;

    public ObserverBean(Object observer, Object sender,
            EventMode threadMode) {
        if (observer == null)
            throw new NullPointerException("ObserverBean cannot be null");
        this.observerReference = new WeakReference<Object>(observer);
        mObserverHashCode = observer.hashCode();
        if (sender != null) {
            this.senderReference = new WeakReference<Object>(sender);
            mSenderHashCode = sender.hashCode();
        } else {
            this.senderReference = null;
            mSenderHashCode = 0;
        }
        this.mThreadMode = threadMode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (mThreadMode.hashCode());
        result = prime * result + mObserverHashCode;
        result = prime * result + mSenderHashCode;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ObserverBean other = (ObserverBean) obj;
        if (mThreadMode != other.mThreadMode)
            return false;
        if (mObserverHashCode != other.mObserverHashCode)
            return false;
        if (mSenderHashCode != other.mSenderHashCode)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ObserverBean [observer=" + getObservingObject() +
                "]";
    }

    public Object getObservingObject() {
        return observerReference.get();
    }

    public Object getEventSourceSender() {
        if (senderReference != null) {
            return senderReference.get();
        }
        return null;
    }

    public void invoke(Object args) {
        final Object observingObject = getObservingObject();
        if (observingObject != null) {
            if (observingObject instanceof IObserver) {
                if (EventMode.PostThread == mThreadMode) {
                    ((IObserver) observingObject).onEventPostThread((Event) args);
                } else if (EventMode.MainThread == mThreadMode) {
                    ((IObserver) observingObject).onEventMainThread((Event) args);
                } else if (EventMode.BackgroundThread == mThreadMode) {
                    ((IObserver) observingObject).onEventBackgroundThread((Event) args);
                } else if (EventMode.Async == mThreadMode) {
                    ((IObserver) observingObject).onEventAsync((Event) args);
                }
            } else {
                Log.e(tag, "fatal error");
            }
        }
    }

}
