
package com.egolden.wangdenghui.eventhandler.event.bean;

/**
 * 事件
 * @author wangdenghui E-mail:WANGDENGHUI351@pingan.com.cn
 * @version 1.1.0
 */
public final class Event {
    /**
     * 具体的事件
     */

    public int what;
    /**
     * 事件源
     */

    public SourceBean source;

    /**
     * 事件对应的参数
     */

    public Object params;
    /**
     * 事件级别
     */

    public EventRank eventRank;

    private Event() {
    }

    @Override
    public String toString() {
        return "Event [what=" + what + ", source=" + source + ", params=" + params + ", eventRank="
                + eventRank + "]";
    }

    public static enum EventRank {
        /**
         * 普通事件，用于广播
         */
        NORMAL,
        /**
         * 系统事件，尽量集中处理
         */
        SYSTEM,
        /**
         * 核心事件，必须集中处理
         */
        CORE
    }

    Event next;
    private static final Object sPoolSync = new Object();
    private static Event sPool;
    private static int sPoolSize = 0;

    private static final int MAX_POOL_SIZE = 50;

    private int mRefrenceTimes = 0;

    public static Event generate() {
        /*
         * synchronized (sPoolSync) { if (sPool != null) { Event e = sPool;
         * sPool = e.next; e.next = null; e.mRefrenceTimes++; sPoolSize--;
         * return e; } }
         */
        return new Event();
    }

    public static Event generate(int what, SourceBean source, Object params, EventRank eventRank) {
        Event e = generate();
        e.what = what;
        e.source = source;
        e.params = params;
        e.eventRank = eventRank;
        e.mRefrenceTimes++;
        return e;
    }

    public static Event generate(int what, SourceBean source, Object params) {
        Event e = generate();
        e.what = what;
        e.source = source;
        e.params = params;
        e.eventRank = EventRank.NORMAL;
        e.mRefrenceTimes++;
        return e;
    }

    public static Event generate(int what, SourceBean source, EventRank eventRank) {
        return generate(what, source, null, eventRank);
    }

    public static Event generate(int what, SourceBean source) {
        return generate(what, source, null, EventRank.NORMAL);
    }

    public void recycle() {
        if (--mRefrenceTimes <= 0) {
            clearForRecycle();

            synchronized (sPoolSync) {
                if (sPoolSize < MAX_POOL_SIZE) {
                    next = sPool;
                    sPool = this;
                    sPoolSize++;
                }
            }
        }
    }

    void clearForRecycle() {
        what = 0;
        source = null;
        params = null;
        eventRank = null;
        mRefrenceTimes = 0;
    }

    public void retain() {
        mRefrenceTimes++;
    }

}
