package com.egolden.wangdenghui.eventhandler.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.egolden.wangdenghui.eventhandler.R;
import com.egolden.wangdenghui.eventhandler.bean.UserInfo;
import com.egolden.wangdenghui.eventhandler.event.EventCenter;
import com.egolden.wangdenghui.eventhandler.event.EventMode;
import com.egolden.wangdenghui.eventhandler.event.IObserver;
import com.egolden.wangdenghui.eventhandler.event.bean.Event;
import com.egolden.wangdenghui.eventhandler.utils.Constans;

/**
 * 观察者
 */
public class ObserverEventActivity extends AppCompatActivity implements IObserver {

    private static final String TAG = ObserverEventActivity.class.getSimpleName();
    private TextView eventResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receive_event);
        eventResult = (TextView) findViewById(R.id.tv_msg_result);
        findViewById(R.id.btn_go_other).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ObserverEventActivity.this, SubjectEventActivity.class);
                startActivity(intent);
            }
        });
        regiest();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventCenter.getInstance().removeObserver(this);
    }

    private void regiest() {
        EventCenter.getInstance().addObserver(EventMode.MainThread, this,
                Constans.LoginEventModel.Name,
                Constans.LoginEventModel.EVENT_LOGIN_IN,
                Constans.LoginEventModel.EVENT_LOGIN_OUT);
    }

    @Override
    public void onEventMainThread(Event event) {
        Log.d(TAG, "onEventMainThread");
        switch (event.what) {
            case Constans.LoginEventModel.EVENT_LOGIN_IN:
                UserInfo userInfo = (UserInfo) event.params;
                eventResult.setText(userInfo.toString());
                break;
            case Constans.LoginEventModel.EVENT_LOGIN_OUT:
                String params = (String) event.params;
                eventResult.setText(params);
                break;
            default:
                break;
        }
    }

    @Override
    public void onEventPostThread(Event event) {
        Log.d(TAG, "onEventPostThread");
    }

    @Override
    public void onEventAsync(Event event) {
        Log.d(TAG, "onEventAsync");
    }

    @Override
    public void onEventBackgroundThread(Event event) {
        Log.d(TAG, "onEventBackgroundThread");
    }
}
