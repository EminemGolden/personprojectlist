
package com.egolden.wangdenghui.eventhandler.event;

/**
 * <pre>
 * 事件执行模式
 * </pre>
 *
 * @author wangdenghui E-mail:WANGDENGHUI351@pingan.com.cn
 * @version 1.1.0
 */
public enum EventMode {
    // 观察的回调函数在发送Event的线程执行
    PostThread,

    // 观察的回调函数在UI线程执行
    MainThread,

    /**
     * 观察的回调函数在非UI线程执行 <1.如果发送Event的线程是UI线程，则在线程池中执行>
     * <2.如果发送Event的线程是非UI，则在发送Event的线程执行>
     */
    BackgroundThread,

    /**
     * 观察的回调函数在后台线程执行 <无论发送线程是什么线程，都把任务放到线程池中执行>
     */
    Async
}
