
package com.egolden.wangdenghui.eventhandler.event.poster;


import com.egolden.wangdenghui.eventhandler.event.EventCenter;
import com.egolden.wangdenghui.eventhandler.event.bean.ObserverBean;
import com.egolden.wangdenghui.eventhandler.event.threadpool.PriorityThreadPool;
import com.egolden.wangdenghui.eventhandler.event.threadpool.ThreadPool;

/**
 * @author wangdenghui E-mail:WANGDENGHUI351@pingan.com.cn
 * @version 1.1.0
 */
public class AsyncPoster implements ThreadPool.Job<Object> {

    private final PendingPostQueue queue;

    private final EventCenter eventBus;

    public AsyncPoster(EventCenter eventBus) {
        this.eventBus = eventBus;
        queue = new PendingPostQueue();
    }

    public void enqueue(ObserverBean subscription, Object event) {
        PendingPost pendingPost = PendingPost.obtainPendingPost(subscription, event);
        queue.enqueue(pendingPost);
        // EventCenter.executorService.execute(this);
        PriorityThreadPool.getDefault().submit(this);
    }

    @Override
    public Object run(ThreadPool.JobContext jc) {
        PendingPost pendingPost = queue.poll();
        if (pendingPost == null) {
            throw new IllegalStateException("No pending post available");
        }
        eventBus.invokeObserver(pendingPost);
        return null;
    }

}
