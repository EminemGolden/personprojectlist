
package com.egolden.wangdenghui.eventhandler.event.poster;

import android.util.Log;

import com.egolden.wangdenghui.eventhandler.event.EventCenter;
import com.egolden.wangdenghui.eventhandler.event.bean.ObserverBean;
import com.egolden.wangdenghui.eventhandler.event.threadpool.PriorityThreadPool;
import com.egolden.wangdenghui.eventhandler.event.threadpool.ThreadPool;


public  final class BackgroundPoster implements ThreadPool.Job<Object> {

    private final PendingPostQueue queue;
    private volatile boolean executorRunning;

    private final EventCenter eventBus;

    public BackgroundPoster(EventCenter eventBus) {
        this.eventBus = eventBus;
        queue = new PendingPostQueue();
    }

    public void enqueue(ObserverBean subscription, Object event) {
        PendingPost pendingPost = PendingPost.obtainPendingPost(subscription, event);
        synchronized (this) {
            queue.enqueue(pendingPost);
            if (!executorRunning) {
                executorRunning = true;
                PriorityThreadPool.getDefault().submit(this);
            }
        }
    }

    @Override
    public Object run(ThreadPool.JobContext jc) {
        try {
            try {
                while (true) {
                    PendingPost pendingPost = queue.poll(1000);
                    if (pendingPost == null) {
                        synchronized (this) {
                            // Check again, this time in synchronized
                            pendingPost = queue.poll();
                            if (pendingPost == null) {
                                executorRunning = false;
                                return null;
                            }
                        }
                    }
                    eventBus.invokeObserver(pendingPost);
                }
            } catch (InterruptedException e) {
                Log.w("Event", Thread.currentThread().getName() + " was interruppted", e);
            }
        } finally {
            executorRunning = false;
        }
        return null;
    }

}
