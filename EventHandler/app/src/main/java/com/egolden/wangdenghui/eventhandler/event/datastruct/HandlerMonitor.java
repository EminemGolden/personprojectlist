package com.egolden.wangdenghui.eventhandler.event.datastruct;

import android.os.Message;
import android.util.Log;
import android.util.Printer;
import android.util.StringBuilderPrinter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;

/**
 * Handler监控
 *
 * @author wangdenghui E-mail:WANGDENGHUI351@pingan.com.cn
 * @version 1.1.0
 */
public class HandlerMonitor {
    private final String tag = "HandlerMonitor";
    private static HandlerMonitor sInstance;
    private HashMap<String, TimeRecoder> mTaskLists = new HashMap<String, TimeRecoder>();
    private final SimpleDateFormat SAVE_INFO_FORMATTER = new SimpleDateFormat(
            "yyyy-MM-dd-HH-mm-ss");
    private final String LOG_PATH = "/tencent/qzone/log";
    private int PERIOD_COUNT = 50000; // 最大监控50000次message消息也可以手动打印
    private int mSort = 0; // 输出的日志排序方式.0:按总消耗时间 1:按调用次数 2:按平均消耗时间

    private HandlerMonitor() {
    }

    public static HandlerMonitor getInstacne() {
        if (null == sInstance) {
            sInstance = new HandlerMonitor();
        }
        return sInstance;
    }

    public void SetSortType(int type) {
        mSort = type;
    }

    public void checkMessage(long startTime, long endTime, BaseHandler handler,
                             Message msg, boolean isSendMsg) {
//		if (!AppConstant.DebugConfig.isDebug) {
//			return;
//		}

        String taskCmd = getMessageCmd(handler, msg, isSendMsg);
        TimeRecoder tr = mTaskLists.get(taskCmd);
        if (tr == null) {
            tr = new TimeRecoder();
            tr.mTaskName = taskCmd;
            mTaskLists.put(taskCmd, tr);
        }
        tr.record(startTime, endTime);

        if (mTaskLists.size() > PERIOD_COUNT) {
            saveLogToSDCard();

            // MultiMailSenderInfo mailInfo =
            // QzoneMailLogSender.getSenderInfo();
            // mailInfo.setMailServerPort("25");
            // mailInfo.setSubject("Business Looper 耗时统计");
            // mailInfo.setContent(sb.toString());
            //
            // QzoneMailLogSender.mailLog(mailInfo, false, null, new
            // OnMailLogListener() {
            // @Override
            // public void onMailLogResult(boolean result) {
            // if
            // (DebugConfig.isDebuggable(QZoneApplication.getInstance().getContext()))
            // {
            // ToastUtils.show(QZoneApplication.getInstance().getContext(),
            // "耗时统计发送" + (result ? "成功" : "失败"));
            // }
            // }
            // }, 1);
        }
    }

    /**
     * 输出日志
     */
    synchronized public void saveLogToSDCard() {
//		if (!AppConstant.DebugConfig.isDebug) {
//			return;
//		}

        StringBuilder sb = new StringBuilder();
        printInfo(new StringBuilderPrinter(sb));
        Log.d(tag, sb.toString());
        mTaskLists.clear();

        String content = sb.toString();

        BufferedWriter bos = null;
        try {
            String state = android.os.Environment.getExternalStorageState();
            if (android.os.Environment.MEDIA_MOUNTED.equals(state)) {
                if (android.os.Environment.getExternalStorageDirectory()
                        .canWrite()) {
                    File file = new File(android.os.Environment
                            .getExternalStorageDirectory().getPath() + LOG_PATH);
                    if (!file.exists()) {
                        file.mkdirs();
                    }
                    String path = file.getAbsolutePath()
                            + "/HandlerMonitor-"
                            + SAVE_INFO_FORMATTER.format(new Date(System
                            .currentTimeMillis())) + ".txt";
                    OutputStreamWriter out = new OutputStreamWriter(
                            new FileOutputStream(path, true), "UTF-8");

                    bos = new BufferedWriter(out);
                    bos.write("\t\n**********************\t\n");
                    android.text.format.Time tmtxt = new android.text.format.Time();
                    tmtxt.setToNow();
                    bos.write(tmtxt.format("%Y-%m-%d %H:%M:%S") + "\n");
                    bos.write(content);
                    bos.write("\t\n");
                    bos.flush();
                    bos.close();
                    bos = null;
                }
            }
        } catch (Exception ebos) {
            ebos.printStackTrace();
        } finally {
            try {
                if (bos != null) {
                    bos.close();
                    bos = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void printInfo(Printer p) {
        if ((null == mTaskLists) || (mTaskLists.size() <= 0))
            return;
        p.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");

        p.println("----------Start Handler Monitor------------");
        ArrayList<TimeRecoder> list = null;
        try {
            list = new ArrayList<TimeRecoder>(mTaskLists.values());
        } catch (ConcurrentModificationException e) {
            Log.e(tag, "printInfo-ConcurrentModificationException");
            e.printStackTrace();
            return;
        }
        switch (mSort) {
            case 1:// 1:按调用次数
                Collections.sort(list, new Comparator<TimeRecoder>() {
                    @Override
                    public int compare(TimeRecoder lhs, TimeRecoder rhs) {
                        return (int) (rhs.mCount - lhs.mCount);
                    }
                });
                break;
            case 2:// 2:按平均消耗时间
                Collections.sort(list, new Comparator<TimeRecoder>() {
                    @Override
                    public int compare(TimeRecoder lhs, TimeRecoder rhs) {
                        return (int) (rhs.mAvgCost - lhs.mAvgCost);
                    }
                });
                break;
            case 0: // 0:按总消耗时间
            default:
                Collections.sort(list, new Comparator<TimeRecoder>() {
                    @Override
                    public int compare(TimeRecoder lhs, TimeRecoder rhs) {
                        return (int) ((rhs.mAvgCost * rhs.mCount) - (lhs.mAvgCost * lhs.mCount));
                    }
                });
                break;
        }

//		if (AppConstant.DebugConfig.isDebug) {
//			Toast.makeText(
//					BaseApplication.getInstance().getBaseContext(),
//					"Handler监控耗时最多的任务  Name:" + list.get(0).mTaskName
//							+ "  AvgCost:" + list.get(0).mAvgCost + "ms",
//					Toast.LENGTH_LONG).show();
//		}

        for (TimeRecoder timeRecoder : list) {
            timeRecoder.printState(p);
        }
        p.println("-----------End Handler Monitor-------------");
    }

    private String getMessageCmd(BaseHandler handler, Message msg,
                                 boolean isSendMsg) {
        String flag = "Thread-"
                + Thread.currentThread().getName()
                + (isSendMsg ? "  Message-Send What:"
                : "  Message-Handler What:");
        try {
            flag += msg.what;
            if (null != msg.getCallback()) {
                flag += " Runnable-" + msg.getCallback().getClass().toString();
            } else if (null != handler.getCallbackEx()) {
                flag += " Callback-"
                        + handler.getCallbackEx().getClass().toString();
            } else {
                flag += " handleMessage-" + handler.getClass().toString();
            }
        } catch (NullPointerException ne) {
            // 打印日志的地方,防止getClass出现null,统一catch
        } catch (Exception e) {

        }

        return flag;
    }

    private class TimeRecoder {
        private String mTaskName;
        private long mMaxTime;
        private long mMaxCost = 0;
        private long mMinTime;
        private long mMinCost = 10000000;
        private long mAvgCost;
        private long mCount;

        private final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss:SSS a");

        public void record(long startTime, long endTime) {
            long time = endTime - startTime;

            if (time >= mMaxCost) {
                mMaxCost = time;
                mMaxTime = startTime;
            }
            if (time <= mMinCost) {
                mMinCost = time;
                mMinTime = startTime;
            }

            mAvgCost = (mAvgCost * mCount + time) / (mCount + 1);
            mCount++;
        }

        public void printState(Printer p) {
            p.println(">>TaskInfo:" + mTaskName);
            p.println("  MaxCost:" + mMaxCost + ",MaxTime:"
                    + DATE_FORMATTER.format(new Date(mMaxTime)));
            p.println("  MinCost:" + mMinCost + ",MinTime:"
                    + DATE_FORMATTER.format(new Date(mMinTime)));
            p.println("  AverageCost:" + mAvgCost + ",count:" + mCount);
        }
    }
}
