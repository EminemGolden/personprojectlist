
package com.egolden.wangdenghui.eventhandler.event;


import com.egolden.wangdenghui.eventhandler.event.bean.Event;

/**
 * <pre>
 * 观察者
 * </pre>
 *
 * @author wangdenghui E-mail:WANGDENGHUI351@pingan.com.cn
 * @version 1.1.0
 */
public interface IObserver {
    /**
     * 观察者的回调接口方法.此接口执行在UI主线程，更新UI资源的行为在此回调执行
     */
     void onEventMainThread(Event event);

    /**
     * 此回调执行在调用线程，可能是UI线程也可能是后台线程
     *
     * @param event
     */

     void onEventPostThread(Event event);

    /**
     * 此回调执行后台线程
     *
     * @param event
     */
     void onEventAsync(Event event);

    /**
     * 如果调用线程是UI线程，则执行在后台线程 如果调用线程不是UI线程，则执行在调用线程
     *
     * @param event
     */
     void onEventBackgroundThread(Event event);
}
