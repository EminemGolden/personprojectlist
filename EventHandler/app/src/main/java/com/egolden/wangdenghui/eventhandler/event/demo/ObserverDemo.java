
package com.egolden.wangdenghui.eventhandler.event.demo;

import android.util.Log;

import com.egolden.wangdenghui.eventhandler.event.EventCenter;
import com.egolden.wangdenghui.eventhandler.event.IObserver;
import com.egolden.wangdenghui.eventhandler.event.bean.Event;


public class ObserverDemo implements IObserver {

    private static final String tag = "ObserverDemo";

    public void init() {
        register();
    }

    private void register() {
        EventCenter.getInstance().addObserver(this,
                DemoEventConstant.DemoModel.Name,
                DemoEventConstant.DemoModel.EVENT_1, DemoEventConstant.DemoModel.EVENT_2);
    }

    public void unRegister() {
        EventCenter.getInstance().removeObserver(this);
    }

    @Override
    public void onEventMainThread(Event event) {
        Log.v(tag, "onEventMainThread params:" + event.params + " what:" + event.what);
    }

    @Override
    public void onEventPostThread(Event event) {

        Log.v(tag, "onEventPostThread:" + event.params + " what:" + event.what);
    }

    @Override
    public void onEventAsync(Event event) {

        Log.v(tag, "onEventAsync:" + event.params + " what:" + event.what);
    }

    @Override
    public void onEventBackgroundThread(Event event) {

        Log.v(tag, "onEventBackgroundThread:" + event.params + " what:" + event.what);
    }

}
