package com.egolden.wangdenghui.eventhandler.event.datastruct;

import android.util.SparseArray;

import java.util.ArrayList;
import java.util.List;

/**
 *  @author wangdenghui E-mail:WANGDENGHUI351@pingan.com.cn
 *
 * @param <E>
 * @version 1.1.0
 */
public class MultiSparseArray<E> {
    private SparseArray<ArrayList<E>> array;

    public MultiSparseArray() {
        array = new SparseArray<ArrayList<E>>();
    }

    public MultiSparseArray(int capacity) {
        array = new SparseArray<ArrayList<E>>(capacity);
    }

    public List<E> get(int key) {
        return array.get(key);
    }

    public E put(int key, E value) {
        if (value == null) {
            return null;
        }
        List<E> list = get(key);
        if (list == null) {
            list = new ArrayList<E>();
            array.put(key, (ArrayList<E>) list);
        }
        int index = list.indexOf(value);
        E oldValue = null;
        if (index != -1) {
            oldValue = list.remove(index);
        }
        list.add(value);
        return oldValue;
    }

    public int keySize() {
        return array.size();
    }

    /**
     * Given an index in the range <code>0...keySize()-1</code>, returns the key from the
     * <code>index</code>th key-value mapping that this SparseArray stores.
     */
    public int keyAt(int index) {
        return array.keyAt(index);
    }

    public List<E> valueAt(int index) {
        return array.valueAt(index);
    }

    public void remove(int key) {
        array.remove(key);
    }

    public void remove(int key, E value) {
        List<E> list = get(key);
        if (list != null) {
            list.remove(value);
        }
    }

    public void clear() {
        array.clear();
    }
}