package com.egolden.wangdenghui.eventhandler.utils;

/**
 * @author wangdenghui E-mail:WANGDENGHUI351@pingan.com.cn
 * @version 1.1.0
 */
public class Constans {

    public final class LoginEventModel{
        public final static String Name = "MSG_EventHandler";
        public static final int EVENT_LOGIN_IN = 1;
        public static final int EVENT_LOGIN_OUT = EVENT_LOGIN_IN + 1;
    }

}
