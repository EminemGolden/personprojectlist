
package com.egolden.wangdenghui.eventhandler.event.demo;

public class DemoEventConstant {
    public final class DemoModel {
        public final static String Name = "DemoModel";

        public final static int EVENT_1 = 0;
        public final static int EVENT_2 = EVENT_1 + 1;
    }
}
