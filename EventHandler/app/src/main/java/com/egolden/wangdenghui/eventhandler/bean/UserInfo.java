package com.egolden.wangdenghui.eventhandler.bean;

import java.io.Serializable;

/**
 * @author wangdenghui E-mail:WANGDENGHUI351@pingan.com.cn
 * @version 1.1.0
 */
public class UserInfo implements Serializable {

    public UserInfo(String userName, String age, String sex, String phoneNo) {
        this.userName = userName;
        this.age = age;
        this.sex = sex;
        this.phoneNo = phoneNo;
    }

    public String userName;
    public String age;
    public String sex;
    public String phoneNo;

    @Override
    public String toString() {
        return "UserInfo{" +
                "userName='" + userName + '\'' +
                ", age='" + age + '\'' +
                ", sex='" + sex + '\'' +
                ", phoneNo='" + phoneNo + '\'' +
                '}';
    }
}
